#include <QCoreApplication>

#include "manage.h"
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Manage m;
    m.init();
    return a.exec();
}
