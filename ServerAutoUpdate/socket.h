#ifndef SOCKET_H
#define SOCKET_H

#include <QObject>
#include <QTcpSocket>

class Socket : public QObject
{
    Q_OBJECT
public:
    explicit Socket(QTcpSocket* socket, int sum);
public slots:
    void socketInit();
private slots:
    void onStateChanged(QAbstractSocket::SocketState socketState);
    void onReadyRead();
    void onSendToClient(QByteArray data);

signals:
    void signalSendToThread(QByteArray data);
    void signalDisconnect(int num);
private:
    QTcpSocket* mySocket;
    QByteArray sourceData;
    int sum;
    int a = 0;
};

#endif // SOCKET_H
